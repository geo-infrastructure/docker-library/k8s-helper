FROM ubuntu:20.04

ARG KUBECTL_VERSION=v1.20.11
ARG HELM_VERSION=v3.8.2
ARG SKAFFOLD_VERSION=v1.38.0

RUN set -exu ;\
    export DEBIAN_FRONTEND=noninteractive ;\
    apt-get update ;\
    apt-get install -y --no-install-recommends \
        curl \
        ca-certificates \
        git \
    ;\
    rm -rf /var/lib/apt/lists/*

RUN set -exu ;\
    curl -sSL -o /usr/local/bin/kubectl \
        "https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl" ;\
    chown root:root /usr/local/bin/kubectl ;\
    chmod +x /usr/local/bin/kubectl

RUN set -ex ;\
    mkdir -p /tmp/helm ;\
    curl -sSL https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz | tar -zxv -C /tmp/helm -f - ;\
    mv /tmp/helm/linux-amd64/helm /usr/local/bin/helm ;\
    chown root:root /usr/local/bin/helm ;\
    chmod +x /usr/local/bin/helm ;\
    rm -rf /tmp/helm ;\
    helm repo add elastic https://helm.elastic.co ;\
    helm repo update

RUN set -ex ;\
    curl -sSL -o /usr/local/bin/skaffold \
        "https://storage.googleapis.com/skaffold/releases/${SKAFFOLD_VERSION}/skaffold-linux-amd64" ;\
    chown root:root /usr/local/bin/skaffold ;\
    chmod +x /usr/local/bin/skaffold
